import matplotlib.pyplot as plt
import numpy as np

# Geting data
with open('target') as f:
    target = f.read()[:-1]

print('Ploting date from' + str( target))
data = np.load(target)

# nomalising data
data = data/max(data[:,1])

#calculateing eclipses
eclipses = data[ data[:,1] < 0.93 ][:,0]

aligned = []
for i in range(len(eclipses)-1):
    if eclipses[i+1] - eclipses[i]  < 0.1:
        aligned.append(i)
eclipses = [eclipses[aligned[j]] for j in range(len(aligned))]
eclipses = [0] + eclipses

# Croping the data
splitdata = []
for i in range(len(eclipses)-1):
    temp = data[ data[:,0] < eclipses[i+1]]
    temp = temp[ temp[:,0] < eclipses[i]]
    splitdata.append( temp )

# ploting data
fig,ax = plt.subplots()
fig.suptitle('Data for one ball eclipsing sun\nsuper posed eclipses')
ax.set_xlabel('Time seconds')
ax.set_ylabel('Nomalised diod signal')
ax.grid()
for section in splitdata:
    print(len(section))
    ax.plot(*section.T, '-')
fig.show()
