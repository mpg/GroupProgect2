import numpy as np
from matplotlib import pyplot as plt

with open('../data/data1') as logfile:
    RawData = [
            row.split('\t') for row in
            logfile.read().split('\n\n')[-1].split('\n')
            ]




TempList = [[
    [RawData[i][0],RawData[i][j]] 
    for i in range(1,len(RawData)-1)]
    for j in range(1,len(RawData[1]))
    ]   

TempSet = []
for chan in TempList:
        TempChanell = []
        for DataPoint in  chan:
            if DataPoint[1]!='-':
                TempChanell.append([float(DataPoint[0]),float(DataPoint[1])])
        TempSet.append(TempChanell)

Channel = np.array(TempSet)

def MakePoint(data):
    x,y = data.transpose()
    average = lambda l: sum(l)/len(l)
    error = lambda L,a: np.sqrt((sum( [ (l-a)**2 for l in L] ) )) /len(L)
    avx,avy = average(x),average(y) 
    erx,ery = error(x,avx),error(y,avx)
    return [avx,avy,erx,ery]



def averaging(TimeSerese):
    data = np.array_split(TimeSerese, 10)
    return [MakePoint(subdata) for subdata in data]

Smoth = np.swapaxes(
        np.array(
            [averaging(Channel[i]) for i in range(len(Channel))]
            )
        , 1,2)

plt.figure()

plt.errorbar(*Smoth[0][:2],
        xerr= Smoth[0][2],
        yerr= Smoth[0][3],
        fmt='-', color='g',
        ecolor='r',elinewidth=1.5,
        capsize=5,
        capthick=2,
        barsabove=True,
        label = 'Averaged version of Channle 0')

#plt.plot(*Smoth[1][:2], '-', label = 'Averaged version of Channle 1')
#plt.plot(*Smoth[2][:2], '-', label = 'Averaged version of Channle 2')
#plt.plot(*Smoth[3][:2], '-', label = 'Averaged version of Channle 3')
#plt.plot(*Smoth[4][:2], '-', label = 'Averaged version of Channle 4')


plt.title(' Plot of preliminary data ')
plt.ylabel('Light Intensity')
plt.xlabel('Time')
plt.xticks()
plt.yticks()
plt.grid()
plt.legend()
plt.show()
