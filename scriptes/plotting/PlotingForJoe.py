import matplotlib.pyplot as plt
import numpy as np


a,b =11.5,11.7
data1 = np.load('../../data/nice1/formated.npy')[:645]
data1 = data1[data1[:,0]<b] 
data1 = data1[data1[:,0]>a] 
data1 = data1/max(data1[:,1])

data2 = np.load('../../data/nice2/formated.npy')[:645]
data2 = data2[data2[:,0]<b] 
data2 = data2[data2[:,0]>a] 
data2 = data2/max(data2[:,1])

data3 = np.load('../../data/nice3/formated.npy')[:645]
data3 = data3[data3[:,0]<b] 
data3 = data3[data3[:,0]>a] 
data3 = data3/max(data3[:,1])

data = [data1,data2,data3]


# ploting data
def subs():
    fig,axs = plt.subplots(2,2)
    axs = axs.flatten()
    axs[0].set_title('Data for one ball eclipsing sun\nsuper posed eclipses')
    for i,dat in enumerate(data):
        axs[0].plot(*dat.T,'*-', label = 'repeat number '+str(i+1))
        axs[i+1].plot(*dat.T,'*-')
        axs[i+1].set_xlabel('Time Seconds')
        axs[i+1].set_ylabel('Nomed brightness')
        axs[i+1].set_title('repeat number '+str(i+1))
        axs[i+1].grid()

    axs[0].legend()
    axs[0].set_xlabel('Time seconds')
    axs[0].set_ylabel('Nomalised diod signal')
    axs[0].grid()

subs()
plt.show()
