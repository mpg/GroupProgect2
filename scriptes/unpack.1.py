import numpy as np
from math import *
import os
import glob
import pickle

    # The Folowing Block Loads data from the Path spesifide in the 
    # Target file located in this directory
    # For example, the target file may contain the string:
    # '../data/priliminary' 
    # The contence is removed from this file and broken in to paragraphs.
    # The last parigraph is broken into lines and the lines into 
    # colomn enterys. The lines (where lines are lists of colomn enterys)
    # are stored in a numpy array called Data

with open('target') as f:
    target = f.read()[:-1]

with open(target) as filename:
    RawData = filename.read().split('\n\n')[-1].split('\n')
    Data = np.array([
        RawData[i].split('\t') 
        for i in range(len(RawData)-1)
        ])

    
    # The Folowing Block Orginises the data into a list of channels,
    # where each channel is a list of points.
    # The ferst element in each point is the time,
    # The second is the mesurement.
    # The labels for the data are placed in a list called ChannelMeta.
    # Such that ChannelMeta[0] contains the lable for Channel[0]

 
ChannelMeta =[ { 'name' : lab } 
        for lab in Data[0][1:]]

Data = Data[1:].transpose()
Channels = np.array([
    [ Data[0],Data[i] ]
    for i in range(1,len(Data))
        ])
Channels = Channels.swapaxes(1,2)

    # The Folowing Block scans throgh each channel 
    # and removes points whos second element is '-'
    # and converts from string too numbers

ShitTest = lambda L: [l[1] != '-' for l in L]
Channels = [
    chan[ ShitTest(chan) ].astype(np.float)  for chan in Channels
    ]
    
    # The folowing Functiion Wrights data to a file for a single Chanle
    # The meta data is stored as well
    # It dose this by makeing a directory with the name of the Channel
    # Using the np.save method to stor the data and the standad 
    # pickle method to stor the meta data
    # It will firest check wether the meta data provided is good

def Dump(chan,meta,fig):
    while input( str(meta)+'\n Is this meta data good? [Y,n] ') =='n':
        new = input(
                'enter update, Eg: \nname = new name\n'
                ).split(' = ')

        meta.update({new[0]:new[1]})


    try:
        os.mkdir('../data/'+meta['name'])
    except FileExistsError:
        pass
    
    np.save('../data/'+meta['name']+'/formated',chan)
   
    with open('../data/'+meta['name']+'/meta','wb') as f:
        pickle.dump(meta,f)
    
    if fig != None:
        fig.savefig('../data/'+meta['name']+'/plot.png')

def Plot(Channels,ChannelMeta,):

    # This Function Chould plot all the channels passed 

    import matplotlib.pyplot as plt
    f = plt.figure()

    # This Block works outwhere to put the sub-plots

    n = floor(sqrt(len(Channels)))
    m = ceil( len(Channels)/ n )

    # This foor loop calls matplotlib on all the Channels

    for i in range(len(Channels)):
        ax = f.add_subplot(n,m,i+1)
        ax.plot(*Channels[i].T,)
        ax.set_ylim(ymin = 0)
        ax.set_title(ChannelMeta[i]['name'])
        ax.grid()   

    f.show()
    return f

fig = None
if input('Would you like to plot this data? [Y,n] ') != 'n':
    fig = Plot(Channels,ChannelMeta)


if input('Would you like to save this data? [Y,n] ') !='n':
    for i in range(len(Channels)):
        Dump(Channels[i],ChannelMeta[i],fig)

