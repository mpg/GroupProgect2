print('Importing')
import numpy as np 
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from  math import *

bigsize = 500
smallsize = 50
NumberOfFrams = 250



# InDisk if a function to check wether a given x,y per is in a disk 
# the peramiters of the disk are center (x0,y0) and radius r
# these perameters should be definde befor the InDisk function is called
InDisk =  lambda x,y: ((x-x0)**2 +(y-y0)**2 <= r**2) 


# Theses Tow blocks of code creat the disk templates for the star and the planet
# The planet uses a not to modify the output of InDisk as its going two absorb the light


print('Defining star')
[x0,y0] = 2*[(bigsize-1)/2]
r = r_star = bigsize/2 - 2*smallsize
star = np.full(( bigsize, bigsize ),None)

for y in range(bigsize):
    for x in range(bigsize):
        star[y][x] = InDisk(x,y)
star = star*1.0
 
print('Defining planet')
[x0,y0] = 2*[(bigsize -1)/2]
r = r_planet =  smallsize/2
planet = np.full(( bigsize,bigsize ),None)
for y in range(bigsize):
    for x in range(bigsize):
        planet[y][x] = not InDisk(x,y)
planet = planet*1

# For each fram its going to de element wise multilication of are planet and star, 
# where the planets has been rolled allong by the loop counter times the stepsize

stepsize = floor(bigsize/NumberOfFrams)
print('frams: ' + str(NumberOfFrams) + '\nstepsize: ' + str(stepsize))

transit = np.full(bigsize, None)
print('Computing transit')


for t in range(0,NumberOfFrams):
    print('Fram number: ' + str(t))
    transit[t] =star*np.roll(planet,int(t*stepsize+bigsize/2))

print('Computing light intensity time serise')
# This makes a time serese of the transit data by adding together all the numbers 
# in each frame. Also there might be Nones in the transit so I filter them out
# With a lambda that checks wether it is a None or not

LightIntensity = list(filter(lambda l: type(l)!=type(None),
                        [ np.sum(fram) for fram in transit]))

print('Generating frams')
# Now its going two create an image object for each fram of the tranit
# It will stor thees object in a list and then use ArtistAnimation two creat a animation from 
# Theese frams

fig, (ax1,ax2) = plt.subplots(1,2)

frams = []
PastIntensity = []
for i in range(len(LightIntensity)):
    im1 = ax1.imshow(transit[i].astype(float))
    im2 = ax2.plot(range(i),PastIntensity,'b')
    frams.append([*im2,im1])
    PastIntensity.append(LightIntensity[i])

print('Stiching frams')
gif = animation.ArtistAnimation(fig, frams,
                                            interval=50,
                                            blit= True)

# Set the x axis for the plot too be as long as the 
# time serise
ax2.set_xlim(   xmin = 0,
                xmax = len(frams))

# Set the y axis for the plot to be bettween zero 
# and the areia of the big cercel
ax2.set_ylim(   ymin = pi* r_star**2 - 2*pi*r_planet**2,
                ymax = pi * r_star**2 +  pi*r_planet**2 )


if input('would you like too Show? [Y,n] ') != 'n':
    print('Showing')
    plt.show()

if input('whould you like two save? [Y,n] ') != 'n':
    gif.save(input('\nwhat would you like to call it? ')+'.mp4', metadata={'artist':'Maxwell Gisborne'})

print('Complet')
