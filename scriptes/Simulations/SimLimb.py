print('Importing')
import numpy as np 
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from  math import *

    
bigsize = 500
smallsize = 50
NumberOfFrams = 250


# InDisk if a function to check wether a given x,y per is in a disk 
# the peramiters of the disk are center (x0,y0) and radius r
# these perameters should be definde befor the InDisk function is called


def LimbMaker(x0,y0,R,c):
    def Limb(x,y):
        rsqr = ((x-x0)**2 + (y-y0)**2)/R**2
        if rsqr < 1:
            return 1-sum([ c[n]*(1-(1-rsqr)**(n/4)) for n in range(len(c))])
        else:
            return 0.0
    return Limb


# Theses Tow blocks of code creat the disk templates for the star and the planet
# The planet uses a not to modify the output of InDisk as its going two absorb the light


def Define(c,offset):
    InDisk =  lambda x,y: 1.0*((x-x0)**2 +(y-y0)**2 <= R**2) 

    print('Defining star')
    [x0,y0] = 2*[(bigsize-1)/2]
    R =  bigsize/2 - 2*smallsize
    Limb = LimbMaker(x0,y0,R,c)
    star = np.full(( bigsize, bigsize ),None)

    for y in range(bigsize):
        for x in range(bigsize):
            star[y][x] = Limb(x,y)
     
    print('Defining planet')
    [x0,y0] = [(bigsize -1)/2, (bigsize -1)/2 -offset]
    R = r_planet =  smallsize/2
    planet = np.full(( bigsize,bigsize ),None)
    for y in range(bigsize):
        for x in range(bigsize):
            planet[y][x] = 1-1* InDisk(x,y)

    return star,planet



def ComputeTransit(star,planet):
    stepsize = floor(bigsize/NumberOfFrams)
    print('frams: ' + str(NumberOfFrams) + '\nstepsize: ' + str(stepsize) + '\nlimb darkaning constants: '+ str(c) )

    transit = np.full(bigsize, None)
    print('Computing transit')

    for t in range(0,NumberOfFrams):
        #print('Fram number: ' + str(t))
        transit[t] =star*np.roll(planet,int(t*stepsize+bigsize/2))


    i=0
    while type(transit[i]) != type(None):
        i=i+1
    return abs(transit[:i])

def ComputeLightCurve(transit):
    print('Computing light intensity time serise')
    # This makes a time serese of the transit data by adding together all the numbers 
    # in each frame. Also there might be Nones in the transit so I filter them out
    # With a lambda that checks wether it is a None or not

    return np.round(list(filter(lambda l: type(l)!=type(None),
                            [ np.sum(fram) for fram in transit]))
                    ,decimals = 4)

c = [0,0,0,1]
R_star = bigsize/2 -2*smallsize
for offset in range(0,int(R_star), int(R_star/10)):
    print('\n'+str(offset))
    star,planet = Define(c,offset)
    transit = ComputeTransit(star,planet)
    LightIntensity = ComputeLightCurve(transit)
    
    fig, (ax1,ax2) = plt.subplots(1,2)
    fig.suptitle('Transiting plannet with offset of ' + str(offset*10/R_star)+'%')
    frams = []
    PastIntensity = []
    for i in range(len(transit)): 
        im1 = ax1.imshow(transit[i].astype(float))
        im2 = ax2.plot(range(len(PastIntensity)),PastIntensity, 'b')
        frams.append([*im2,im1])
        PastIntensity.append(LightIntensity[i])

    print('Stiching frams')
    gif = animation.ArtistAnimation(fig, frams,
                                                interval=50,
                                                blit= True)

    # Set the x axis for the plot too be as long as the 
    # time serise
    ax2.set_xlim(   xmin = 0,
                    xmax = len(frams))

    print('Saveing')
    gif.save('Limb darkening c = [0,0,0,1], offset = '+str(offset)+'.mp4')

print('Complet')
