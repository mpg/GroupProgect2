import matplotlib.pyplot as plt
import numpy as np



data1 = np.load('../../data/nice1/formated.npy')[:645]
data2 = np.load('../../data/nice2/formated.npy')[:645]
data3 = np.load('../../data/nice3/formated.npy')[:645]
data = [data1,data2,data3]

avdata = sum(data)/3


# ploting data
def subs():
    fig,axs = plt.subplots(2,2)
    axs = axs.flatten()
    axs[0].set_title('Data for one ball eclipsing sun\nsuper posed eclipses')
    for i,dat in enumerate(data):
        axs[0].plot(*dat.T, label = 'repeat number '+str(i+1))
        axs[i+1].plot(*dat.T)
        axs[i+1].set_xlabel('Time Seconds')
        axs[i+1].set_ylabel('Nomed brightness')
        axs[i+1].set_title('repeat number '+str(i+1))
        axs[i+1].grid()

    axs[0].legend()
    axs[0].set_xlabel('Time seconds')
    axs[0].set_ylabel('Nomalised diod signal')
    axs[0].grid()

def av():
    fig,ax = plt.subplots()
    ax.plot(*avdata.T)
    ax.grid()
    ax.set_title('Averaged')

